from django.apps import AppConfig


class GwbnyappConfig(AppConfig):
    name = 'gwbnyapp'
