from django.shortcuts import render
from django.views import generic
from .models import Answer, Question

# Create your views here.

class HomeView(generic.ListView):
	"""docstring for HomeView"""
	model = Question
	template_name = "gwbnyapp/index.html"
	def get_queryset(self):
		return Question.objects.all()
	