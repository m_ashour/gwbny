from django.db import models

# Create your models here.

class Category(models.Model):
	"""docstring for Category"""
	name =  models.CharField(max_length = 127)

	def __str__(self):
		return self.name


class User(models.Model):
	"""docstring for User"""
	user_name = models.CharField(max_length = 127)
	user_password = models.CharField(max_length = 30)
	user_mail = models.CharField(max_length = 127)
	user_bio = models.TextField()

	def __str__(self):
		return self.user_name

		

class Question(models.Model):
	"""docstring for Question"""
	#many to one relationship
	q_user = models.ForeignKey(User)
	q_category = models.ForeignKey(Category,  on_delete=models.CASCADE)
	q_title = models.CharField(max_length = 200)
	q_text = models.TextField()
	q_likes = models.IntegerField(default  = 0)
	q_date = models.DateTimeField('date published')

	def __str__(self):
		return self.q_title

class Answer(models.Model):
	"""docstring for Answer"""
	ans_user = models.ForeignKey(User)
	ans_question = models.ForeignKey(Question, on_delete=models.CASCADE)
	ans_text = models.TextField()
	ans_likes = models.IntegerField(default = 0)
	ans_date = models.DateTimeField('date published')
	def __str__(self):
		return self.ans_text


